from vm_factory import slice_part_instantiation
from vm_factory import vm_manager

def clone_test(vms):
    print("Testing Clone")
    for vm in vms:
        if(vm_manager.clone_vm(vm["template"], vm["name"]) == 0):
                print("Stopping vm clone. An error has occurred")
                return 0
    return 1

def delete_test(vms):
    for vm in vms:
        if(vm_manager.vm_exists(vm["name"])):
            vm_manager.delete_vm(vm["name"])
    return 1

def start_test(vms):
    print("Starting slice_part vms")
    for vm in vms:
        if(vm_manager.vm_exists(vm["name"])):
            vm_manager.start_vm(vm["name"])
        else:
            print(f"ERROR: vm '" + vm["name"]+ "' not found")
            return 0
    return 1

def stop_test(vms):
    print("Stopping slice_part vms")
    for vm in vms:
        if(vm_manager.vm_exists(vm["name"])):
            vm_manager.shutdown_vm(vm["name"])
        else:
            print(f"ERROR: vm '" + vm["name"]+ "' not found")
            return 0
    return 1


# Declaring vms
vms = [{"name":"master-dcsc", "template":"kube-master-template"},
    {"name":"worker1-dcsc", "template":"kube-worker-template"},
    {"name":"worker2-dcsc", "template":"kube-worker-template"}]

#clone_test(vms)
#delete_test(vms)
#start_test(vms)
#stop_test(vms)
#vm_manager.list_active_vms()