from flask import Flask, Blueprint, request
from model.slice_part import SlicePart
from model.vm import Vm
from model.vim import Vim
from model.controller import Controller
from dao.slice_part_dao import SlicePartDAO
from dao.vm_dao import VmDAO
from dao.vim_dao import VimDAO
from dao.user_dao import UserDAO
from dao.controller_dao import ControllerDAO
from dao.update_slice_part_dao import UpdateSlicePartDAO
from dao.vim_type_dao import VimTypeDAO
from slice_creator.handle_slice_creator import HandleSliceCreator
from slice_creator import validate_yaml
from vm_factory import slice_part_instantiation
from ruamel.yaml import YAML
import json, yaml, logging, _thread
from slice_creator import logs
from datetime import datetime
from vm_factory import slice_part_instantiation

slice_part = Blueprint('slice_part', 'slice_part', url_prefix='/slice_part')
slice_creator = HandleSliceCreator()
slice_part_dao = SlicePartDAO()
controller_dao = ControllerDAO()
vm_dao = VmDAO()
vim_dao = VimDAO()
vim_type_dao = VimTypeDAO()
user_dao = UserDAO()
update_slice_dao = UpdateSlicePartDAO()
vm = Vm()

@slice_part.route('', methods=['GET'])
def get():
    try:
        yaml.load(request.data)
    except Exception as e:
        logs.logger.error(e)
        logs.logger.error("Invalid yaml file!")
        return "-1", 404

    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    id = None
    if(slice_part["uuid"] != None): id = slice_part["uuid"]
    # Get all Slice Part if uuid are null
    if(id == None):
        obj = SlicePart()
        result = slice_part_dao.select_all_slice_parts()
        if(result): 
            return yaml.dump([obj.__dict__ for obj in result]).replace("_SlicePart__", "").replace("!!python/object:model.vm.Vm", "").replace("{", "VM:\n    ").replace("_Vm__", "").replace(", ", ",\n    ").replace("}", "")

        else: 
            logs.logger.error("Failed to get slice part")
            return "-1", 404
    # Get a User
    slice_part = slice_part_dao.select_slice_part(id)
    if(slice_part): return yaml.dump(slice_part), 200 
    else: 
        logs.logger.error(slice_part_dao.get_msg())
        return "-1", 404

@slice_part.route('request', methods=['POST'])
def post():
    # validate yaml
    try:
            yaml.load(request.data)
    except Exception as e:
        logs.logger.error("Invalid yaml file!")
        return "-1", 404

    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    # validate yaml
    if(validate_yaml.valid_fields_post_slice(slice_part)):
        pass
    else: 
        logs.logger.error("Invalid yaml file!")
        return "-1", 404
    # looking for controller id
    controller = controller_dao.select_all_controllers()
    if(controller == 0 ):
        logs.logger.error("No registered controller")
        return "-1", 404
    #instantiating vim_obj, slice_part_obj
    vim_obj = Vim(None, None, slice_part["VIM"]["VIM_Type_name"])
    status = 'accepted'
    slice_part_obj = SlicePart(slice_part["name"], None, None, slice_part["user"],  vim_obj.get_uuid(), controller.get_id() , None, None, status)
    # converting json array from vms to array object from vms
    slice_part_vms = vm.to_object(slice_part["vdus"], slice_part_obj.get_uuid())
    if(slice_part_vms):
        pass
    else:
        logs.logger.error("Template selected does not exist")
        return "-1", 404
    # verifying whether vms can be allocated (vms = 0 if vms can not be allocated)
    vms = slice_creator.distribute_vms_insert(slice_part_vms, slice_part_obj.get_valid_from(), slice_part_obj.get_valid_until())
    if(vim_type_dao.select_vim_type(slice_part["VIM"]["VIM_Type_name"])):
        pass
    else:
        logs.logger.error("VIM Type selected does not exist")
        return "-1", 404
    if(vms != 0):
        if(vim_dao.insert_vim(vim_obj)):
            if(slice_part_dao.insert_slice_part(slice_part_obj)): 
                for row in vms:
                    if(vm_dao.insert_vm(row)):
                        slice_part_obj.set_vms(slice_part_dao.select_slice_part_vms(slice_part_obj.get_uuid()))
                    else:
                        msg = vm_dao.get_msg()
                        vm_dao.truncate_vm(slice_part_obj.get_uuid())
                        slice_part_dao.delete_slice_part(slice_part_obj.get_uuid())
                        vim_dao.delete_vim(vim_obj.get_uuid())
                        logs.logger.error(msg)
                        return "-1", 404
                # Success
                logs.logger.info("slice_part uuid = " + slice_part_obj.get_uuid())
                return slice_part_obj.get_uuid(), 201
            else: 
                vim_dao.delete_vim(vim_obj.get_uuid())
                logs.logger.error(slice_part_dao.get_msg())
                return "-1", 404
        else: 
            logs.logger.error(vim_dao.get_msg())
            return "-1", 404
    logs.logger.error("No resources available to insert VMS")
    return "-1", 404
    

@slice_part.route('', methods=['PUT'])
def put():
    try:
            yaml.load(request.data)
    except Exception as e:
        logs.logger.error(e)
        logs.logger.error("Invalid yaml file!")
        return "-1", 404
    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    #looking for controller id
    controller = controller_dao.select_all_controllers()
    if(controller == 0 ):
        logs.logger.error("No registered controller")
        return "-1", 404

    if(vim_dao.select_vim(slice_part["vim_uuid"]) == 0):
        logs.logger.error(vim_dao.get_msg())
        return "-1", 404
    slice_part_obj = SlicePart(slice_part["name"], None, None, slice_part["user"], slice_part["vim_uuid"], controller.get_id(), None, None)
    slice_part_obj.set_uuid(slice_part["uuid"])
    old_slice_part = slice_part_dao.select_slice_part(slice_part_obj.get_uuid())
    if(old_slice_part == 0):
        logs.logger.error(slice_part_dao.get_msg())
        return "-1", 404
    slice_part_obj.set_status(old_slice_part.get_status())
    if(slice_part_dao.update_slice_part(slice_part_obj)):
        return slice_part_obj.get_uuid(), 201
    else: 
        logs.logger.error(update_slice_dao.get_msg())
        return "-1", 404
    
@slice_part.route('', methods=['DELETE'])
def delete():
    try:
        # parsing yaml
        slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    except Exception as e:
        return "Invalid yaml file!", 404
    id = None
    # check file
    if(slice_part["uuid"] != None): id = slice_part["uuid"]
    # get slice
    slice_part_obj = slice_part_dao.select_slice_part(id)
    if(slice_part_obj):
        # delete hypervisor_vms
        if(slice_part_instantiation.delete_slice_part_vms(slice_part_obj) == 0):
            return "-1", 404
        # delete slice_part of database
        if(vm_dao.truncate_vm(id)):
            if(slice_part_dao.delete_slice_part(id)): 
                if(vim_dao.delete_vim(slice_part_obj.get_vim_uuid())):
                    logs.logger.info(slice_part_dao.get_msg())
                    return id, 200
                else: 
                    logs.logger.error(vim_dao.get_msg())
                    return "-1", 404
            else: 
                logs.logger.error(slice_part_dao.get_msg())
                return "-1", 404
        else: 
            logs.logger.error(vm_dao.get_msg())
            return "-1", 404
    else: 
        logs.logger.error(slice_part_dao.get_msg())
        return "-1", 404


@slice_part.route('activate', methods=['POST'])
def activate():
    # parse to object
    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    slice_part_obj = slice_part_dao.select_slice_part(slice_part["uuid"])
    # search slice_part
    if(slice_part_obj.get_uuid()):
        #deploy 
        if (slice_part_instantiation.deploy_slice_part(slice_part_obj) != 1): return "-1", 404
        if (slice_part_instantiation.start_slice_part_vms(slice_part_obj) != 1): return "-1", 404
        return slice_part_obj.get_uuid(), 201
    else: return "-1", 404

@slice_part.route('stop', methods=['POST'])
def stop():
    # parse to object
    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    slice_part_obj = slice_part_dao.select_slice_part(slice_part["uuid"])
    # search slice_part
    if(slice_part_obj.get_uuid()):
        #deploy 
        if (slice_part_instantiation.stop_slice_part_vms(slice_part_obj) != 1): return "-1", 404
        return slice_part_obj.get_uuid(), 201
    else: return "-1", 404

@slice_part.route('start', methods=['POST'])
def start():
    # parse to object
    slice_part = json.loads(json.dumps(yaml.load(request.data)))["dc-slice-part"]
    slice_part_obj = slice_part_dao.select_slice_part(slice_part["uuid"])
    # search slice_part
    if(slice_part_obj.get_uuid()):
        if (slice_part_instantiation.start_slice_part_vms(slice_part_obj) != 1): return "-1", 404
        return slice_part_obj.get_uuid(), 201
    else: return "-1", 404

