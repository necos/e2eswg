import logging
from settings import log_file

logging.getLogger('').handlers = []
logging.getLogger('app_server').setLevel(logging.DEBUG)
FORMAT = '%(asctime)s - %(funcName)s - %(levelname)s - %(message)s' 
logging.basicConfig(filename=log_file, filemode='w', format=FORMAT)
logger = logging.getLogger('AppServer')
logger.setLevel(logging.DEBUG)