from vm_factory.connection import uri, xen_user, xen_host, xen_port
from slice_creator import logs 
import libvirt
import sys, os

def start_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.create()
        logs.logger.info(f"Starting vm '{name}'")
    conn.close()

def shutdown_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name) 
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.shutdown()
        logs.logger.info(f"Shutting Down vm '{name}'")
    conn.close()

def destroy_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.destroy()
        logs.logger.info(f"Destroying vm '{name}'")
    conn.close()

def suspend_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.suspend()
        logs.logger.info(f"Suspending vm '{name}'")
    conn.close()

def resume_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.resume()
        logs.logger.info(f"Resuming vm '{name}'")
    conn.close()

def delete_vm(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.undefine()
        os.system(f"ssh {xen_user}@{xen_host} -p {xen_port} 'sudo lvremove --force /dev/vg0/{name}-disk'")
        logs.logger.info(f"Vm '{name}' deleted successfully")
    conn.close()

def clone_vm(origin_vm, new_name):
    command = f"virt-clone --connect={uri} -o {origin_vm} -n {new_name} -f /dev/vg0/{new_name}-disk --force"
    conn = libvirt.open(uri)
    vm = conn.lookupByName(origin_vm)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {origin_vm}", file=sys.stderr)
        return 0
    conn.close()
    logs.logger.info(f"Clonning {origin_vm} to {new_name}...")
    os.system(command)
    logs.logger.info(f"{new_name} created successfully!")
    return 1


def set_vcpu(name, vcpus_amount):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.setVcpus(vcpus_amount)
        logs.logger.info(f"Vcpus updated successfully!")
    conn.close()

def set_memory(name, memory_amount):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        logs.logger.info(f"Failed to get the domain object {name}", file=sys.stderr)
        return 0
    else: 
        vm.setMemory(memory_amount)
        logs.logger.info(f"Memory updated successfully!")
    conn.close()

def list_active_vms():
    conn = libvirt.open(uri)
    vms = conn.listDomainsID()
    logs.logger.info("Active vms:")
    for vm_id in vms:
        vm = conn.lookupByID(vm_id)
        logs.logger.info(vm.name())
    conn.close()

def list_all_vms():
    conn = libvirt.open(uri)
    domainNames = conn.listDefinedDomains()
    if conn == None:
        logs.logger.info('Failed to get a list of domain names', file=sys.stderr)

    domainIDs = conn.listDomainsID()
    if domainIDs == None:
        logs.logger.info('Failed to get a list of domain IDs', file=sys.stderr)
    if len(domainIDs) != 0:
        for domainID in domainIDs:
            domain = conn.lookupByID(domainID)
            domainNames.append(domain.name)

    logs.logger.info("All (active and inactive domain names:")
    if len(domainNames) == 0:
        logs.logger.info('  None')
    else:
        for domainName in domainNames:
            logs.logger.info('  ' + str(domainName))

    conn.close()


def vm_exists(name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(name)
    if vm == None: 
        return 0
    else:
        return 1

def add_device(vm_name, xml):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(vm_name)
    # create new device by xml 
    vm.attachDeviceFlags(xml)

def remove_device(vm_name, xml):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(vm_name)
    # create new device by xml 
    vm.detachDeviceFlags(xml)

def get_xml(vm_name):
    conn = libvirt.open(uri)
    vm = conn.lookupByName(vm_name)
    xml_desc = vm.managedSaveGetXMLDesc()
    logs.logger.info(xml_desc)