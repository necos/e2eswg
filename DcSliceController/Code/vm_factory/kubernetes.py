from tools.ssh import SSH
from settings import kube_credencials
from vm_factory import vm_manager
from dao.slice_part_dao import SlicePartDAO
from slice_creator import logs
from network_manager import network_manager
from settings import bridge_control
import random, time

def config_vms(slice_part):
    vms = slice_part.get_vms()
    join_cmd = None
    master_eth1 = None
    vms.sort()
    bridge = network_manager.create_resource(slice_part.get_uuid())
    for vm in vms:
        logs.logger.info("Configuring vm " + vm.get_name_yaml()+ " - " + vm.get_type())
        # add interfaces
        mac_eth1 = add_interfaces(vm.get_name_hypervisor(), bridge)
        # start vm
        vm_manager.start_vm(vm.get_name_hypervisor())
        # connect to template 
        con = SSH(vm.get_template().get_ip_address(), kube_credencials["username"], kube_credencials["password"]) 
        # change hostname
        logs.logger.info("Changing vm hostname to " + vm.get_name_yaml())
        con.exec_cmd(f"sed -i 's/MYHOSTNAME/{vm.get_name_yaml()}/g' /root/change_hostname.sh")
        con.exec_cmd("./change_hostname.sh")
        # change ip_address
        con.exec_cmd(f"sed -i 's/10.10.10.60/{vm.get_ip_address()}/g' /etc/netplan/01-netcfg.yaml")
        con.exec_cmd(f"sed -i 's/#//g' /etc/netplan/01-netcfg.yaml")        
        # check vm_type
        if(vm.get_type() == "master"):
            master_eth1 = mac_eth1
            # change master ip
            ip = kube_credencials["ip"]
            ip2 = kube_credencials["ip2"]
            con.exec_cmd(f"sed -i 's/{ip}/{ip2}/g' /etc/netplan/01-netcfg.yaml")
            con.exec_cmd(f"sed -i 's/#//g' /etc/netplan/01-netcfg.yaml")
            # apply network changes 
            con.exec_cmd("netplan apply")
            con.close()
            # connect to new ip
            con = SSH(kube_credencials["ip2"], kube_credencials["username"], kube_credencials["password"])
            con.exec_cmd("ifconfig")
            # setup master
            con.exec_cmd(f"sed -i 's/MYIP/{vm.get_ip_address()}/g' /root/install_pod.sh")
            con.exec_cmd("./install_pod.sh")
            join_cmd = con.exec_cmd("sudo kubeadm token create --print-join-command")
            logs.logger.info(join_cmd)
            if(len(join_cmd) < 20):
                logs.logger.info("ERROR: Join command invalid:")
                slice_part.set_status("failed")
                network_manager.remove_resource(slice_part.get_uuid())
                if(SlicePartDAO().update_slice_part(slice_part) != 1): return 0
                return 0
        elif (vm.get_type() == "worker"):
            # apply network changes
            con.exec_cmd("netplan apply")
            if(join_cmd != None):
                # Setup worker
                logs.logger.info("Executing join command ...")
                logs.logger.info(join_cmd)
                con.exec_cmd(join_cmd + " >> kube-join.log")
                # deplay to join configure
                time.sleep(20)
            else:
                # Call function to get join_cmd
                pass
        # Remove configuration scripts
        con.exec_cmd("rm /root/change_hostname.sh")
        con.exec_cmd("rm /root/install_pod.sh")
        con.close()
        # Shutdown VM and remove eth1
        if(vm.get_type() == "worker"):
            logs.logger.info("Finishing configuration ...")
            vm_manager.shutdown_vm(vm.get_name_hypervisor())
            time.sleep(5)
            remove_eth1(vm.get_name_hypervisor(), mac_eth1)
        logs.logger.info(f"Configuration of vm {vm.get_name_yaml()} complete!")
    logs.logger.info("Turning off master ...")
    time.sleep(10)
    vm_manager.shutdown_vm(vms[0].get_name_hypervisor())
    time.sleep(10)
    remove_eth1(vms[0].get_name_hypervisor(), master_eth1)
    logs.logger.info(f"Configuration of slice_part {slice_part.get_name()} complete!")
    return 1

def add_interfaces(vm_name, bridge_eth0):
    #create interface of slice
    xml_eth0 = f"""
        <interface type='bridge'>
        <source bridge='{bridge_eth0}'/>
        <virtualport type='openvswitch'/>
        <script path='vif-openvswitch'/>
        </interface>
    """
    vm_manager.add_device(vm_name, xml_eth0)
    logs.logger.info("Interface eth0 added")

    # create interface of control
    mac_eth1 = rand_mac()
    xml_eth1 = f"""
        <interface type='bridge'>
        <mac address='{mac_eth1}'/>
        <source bridge='{bridge_control}'/>
        </interface>
    """
    vm_manager.add_device(vm_name, xml_eth1)
    logs.logger.info("Interface eth1 added")
    # returns mac of control
    return mac_eth1

def remove_eth1(vm_name, mac): 
    xml_eth1 = f"""
        <interface type='bridge'>
        <mac address='{mac}'/>
        <source bridge='{bridge_control}'/>
        </interface>
    """
    vm_manager.remove_device(vm_name, xml_eth1)
    return 

def rand_mac():
    mac = [ 0x00, 0x16, 0x3e,
		random.randint(0x00, 0x7f),
		random.randint(0x00, 0xff),
		random.randint(0x00, 0xff) ]
    return ':'.join(map(lambda x: "%02x" % x, mac)) 